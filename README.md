![remarkBlue.png](https://bitbucket.org/repo/KB9RXr/images/2260662913-remarkBlue.png)

# Demo code used for meetup #
http://www.meetup.com/Hot-Waters-Web-Developers/events/230871898/

# Meetup video available at #
https://www.youtube.com/watch?v=UNPqVg-Nnc4

# Deployed on Heroku #
http://blooming-anchorage-37997.herokuapp.com/users